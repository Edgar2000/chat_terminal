<?php

    return [
        'email' => [
            'class' => 'SendToEmailMessages',
            'mandatory' => [],
            'related' => [
                '--bcc',
                '--cc',
            ]
        ],
    ];
