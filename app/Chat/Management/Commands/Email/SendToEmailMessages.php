<?php

namespace App\Chat\Management\Commands\Email;

use App\Chat\Support\Clean\ToClean;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class SendToEmailMessages
{
    use ToClean;

    public $message = null;
    public $message_arr = null;
    public $info = null;

//    public $text_cipher = '--text=';
    public $subject_cipher = '--subject=';

    public $email = null;
    public $text = null;
    public $subject = null;
    public $bcc_cc = false;

    public $fun_arr = [
        'email send' => 'sendMessage',
    ];


    /**
     * @param $str
     * @param $arr
     * @param $info
     * @return bool
     */
    public function establish($str, $arr, $info): bool
    {
        $this->redistribution($str, $arr, $info);
        $this->getCost();

        foreach ($this->fun_arr as $fun_key => $fun_val)
        {
            switch ($fun_key) {
                case Str::contains($this->message, $fun_key):
                        $this->$fun_val();
                    break;
            }
        }
        return true;
    }


    /**
     * @param $message
     * @param $message_arr
     * @param $info
     */
    public function redistribution($message, $message_arr, $info)
    {
        $this->message = $message;
        $this->message_arr = $message_arr;
        $this->info = $info;
    }


    /**
     * @return bool
     */
    public function getCost(): bool
    {
        $em = (isset($this->message_arr[2]) && !empty($this->message_arr[2])) ? $this->message_arr[2] : null;
        $em_arr = str_split($em);
        $check_em = $em_arr[0] == '"' && end($em_arr) == '"';
        $emails = [];
        $fc = $this->full_clean($this->message_arr[2]);
        if(Str::contains($this->message_arr[2], ',')) $emails = explode(',' , $fc);

        if($emails)
        {
            foreach ($emails as $item_email)
            {
                if($check_em && !empty($item_email) && filter_var($item_email, FILTER_VALIDATE_EMAIL)) $this->email[] = $item_email;
            }
        } else {
            $this->email = ($check_em && !empty($em) && filter_var($this->full_clean($this->message_arr[2]), FILTER_VALIDATE_EMAIL)) ? $this->full_clean($this->message_arr[2]) : null;
        }


        foreach ($this->message_arr as $mess)
        {
            switch ($mess) {
                case str_contains($mess, $this->subject_cipher):
                    $sbj = explode($this->subject_cipher, $mess) ?? [];
                    $sbj_arr = str_split($sbj[1]);
                    $check_sbj = $sbj_arr && $sbj_arr[0] == '"' && end($sbj_arr) == '"';
                    $this->subject = ($check_sbj && count($sbj) > 1) ? $this->full_clean($sbj[1]) : null;
                    break;
            }
        }

        $txt = (isset($this->message_arr[4]) && !empty($this->message_arr[4])) ? $this->message_arr[4] : null;
        $txt_arr = str_split($txt);
        $check_txt = $txt_arr[0] == '"' && end($txt_arr) == '"';
        $this->text = ($check_txt) ? $this->full_clean($txt) : null;

        if(isset($this->info['related']) && !empty($this->info['related']))
        {
            $bcc_cc = false;
            if(Str::contains($this->message, '--bcc --cc')) $bcc_cc = '--cc';
            if(Str::contains($this->message, '--cc --bcc')) $bcc_cc = '--bcc';
            if(!$bcc_cc)
            {
                foreach ($this->info['related'] as $rel)
                {
                    if(Str::contains($this->message, $rel)) $bcc_cc = $rel;
                }

            }
            if($bcc_cc) $this->bcc_cc = $bcc_cc;
        }

        return true;
    }


    /**
     * @return bool
     */
    public function sendMessage(): bool
    {
        if (isset($this->email) && !is_null($this->email))
        {
            $data = array('email' => $this->email, 'subject' => $this->subject, 'text' => $this->text);
            Mail::send('mail', $data, function($send) {
                $send->from('ed.arm.2000@gmail.com', Auth::user()->email);
                if (!$this->bcc_cc)
                    $send->to($this->email, $this->email);
                if ($this->bcc_cc && $this->bcc_cc == '--bcc')
                    $send->bcc($this->email, $this->email);
                if ($this->bcc_cc && $this->bcc_cc == '--cc')
                    $send->cc($this->email, $this->email);
                $send->subject($this->subject);
            });
            dd($data);
        }
        return true;
    }
}
